# Lexique

## Noeuds

Serveurs physiques ou virtuels.

## Pods

Ensemble cohérent de conteneurs (un ou plusieurs). C'est une instance de K8s.

## Namespaces

Cluster virtuel qui représente un sous ensemble pour cloisonner K8S.

## Service

Abstraction de pods, permet d'éviter la communication par IP/port.

## Deployments

Objet de gestion des déploiments (création / suppression).

## Scaling

Gestion de paramètres pour la montée en charge ou non.

## Kubeadm

Installation du cluster.

## Kubelet

Service qui tourne sur les machines et lance les pods.

## Kubectl

Permet la communication avec le cluster.
