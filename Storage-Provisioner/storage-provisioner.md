# Commandes pour appliquer les PVC

## Default 

```
kubectl create -f pvc-default.yaml
persistentvolumeclaim/local-path-pvc created
```

Pour vérifier que cela fonctionne :

```
kubectl create -f volume-test-pod.yaml
pod/volume-test created
```

Résultat :

```
kubectl get pvc
NAME             STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
local-path-pvc   Bound    pvc-4e727170-1d3f-4def-956e-06144074836c   10Gi       RWO            local-path     4m17s
```
