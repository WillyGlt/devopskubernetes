# Installation de HELM Nginx

## Ajout du repo

```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
"nginx-stable" has been added to your repositories
```

## Mise à jour du repo

```
helm repo update
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "ingress-nginx" chart repository
```

## Installation de Nginx

```
helm install nginx ingress-nginx/ingress-nginx
```
