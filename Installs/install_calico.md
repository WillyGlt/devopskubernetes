# Installation de Calico

Après le Kubeadm init voici une succession de commande à réaliser.

`kubectl create -f https://docs.projectcalico.org/manifests/tigera-operator.yaml`

`kubectl create -f https://docs.projectcalico.org/manifests/custom-resources.yaml`

Pour vérifier que ça fonctionne : `watch kubectl get pods -n calico-system`

```
NAME                                       READY   STATUS    RESTARTS   AGE
calico-kube-controllers-5c6f449c6f-sjfh9   1/1     Running   0          64s
calico-node-m2skk                          1/1     Running   0          65s
calico-typha-6854df855d-sc72w              1/1     Running   0          65s
```

On supprime les taints :

`kubectl taint nodes --all node-role.kubernetes.io/master-`