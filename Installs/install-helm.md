# Install HELM

## Ajout de la clé APT

`curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -`

Résultat :

```
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  1700  100  1700    0     0   6910      0 --:--:-- --:--:-- --:--:--  6910
OK
```

## Ajout du repo dans les listes sources

`echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list`

Résultat :

`deb https://baltocdn.com/helm/stable/debian/ all main`

## Mise à jour des repos

`sudo apt-get update`

## Installation de HELM

`sudo apt-get install helm`

## Terminé !

```
helm version
version.BuildInfo{Version:"v3.4.0", GitCommit:"7090a89efc8a18f3d8178bf47d2462450349a004", GitTreeState:"clean", GoVersion:"go1.14.10"}
```
