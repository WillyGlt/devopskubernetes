# Installation de Cert Manager

## Installation des CRDs

`kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.1.0/cert-manager.crds.yaml`

## Ajout du repo

`helm repo add jetstack https://charts.jetstack.io`

## Installation de Cert Manager

`helm install cert-manager jetstack/cert-manager`
