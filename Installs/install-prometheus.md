# Installation de Prometheus

## Ajout du rep

`helm repo add prometheus-community https://prometheus-community.github.io/helm-charts`

## Mise à jour de la liste

`helm repo update`

## Installation de Prometheus

`helm install prometheus prometheus-community/kube-prometheus-stack`
