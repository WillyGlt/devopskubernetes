# Installation RBAC

## Ajout du repo

`helm repo add fairwinds-stable https://charts.fairwinds.com/stable`

## Installation de la chart

`helm install rbac-manager fairwinds-stable/rbac-manager --namespace rbac-manager`
