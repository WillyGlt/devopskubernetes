# Installation de Kubeadm

(mais aussi de Kubelet et Kubectl)

## Prérequis 

Faire les mises à jour :

`sudo apt update && sudo apt update`

Installer les outils communs :

`sudo apt-get install software-properties-common` (pour le add-apt-repository notamment)

Installer Curl :

`sudo apt install curl`

Installer le gestionnaire de dépendances Debian:

`sudo apt install apt-transport-https`

On installe l'intégration de OpenPGP `gnupg2` :

`sudo apt install gnupg2`

## Installation

Téléchargement et ajout de la clé APT :

`curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -`

On ajoute le repo APT de Kubernetes :

`echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list`

On update afin d'actualiser la liste des repos :

`sudo apt-get update` 

```shell
Get:4 https://packages.cloud.google.com/apt kubernetes-xenial InRelease [8993 B]
Get:5 https://packages.cloud.google.com/apt kubernetes-xenial/main amd64 Packages [41.3 kB]
```

On installe Kubeadm, Kubectl et Kubelet :

`sudo apt-get install -y kubelet kubeadm kubectl`

On marque les trois paquets sur `hold` pour éviter qu'ils soient update automatiquement :

`apt-mark hold kubelet kubeadm kubectl`

On initialise kubeadm

`sudo kubeadm init --pod-network-cidr=192.168.0.0/16 --apiserver-advertise-address=173.212.215.95`

Une fois l'initialisation effectuée, il faut lancer ces trois commandes en tant qu'utilisateur classique.

`mkdir -p $HOME/.kube`
`sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config`
`sudo chown $(id -u):$(id -g) $HOME/.kube/config`

Lors de l'installation nous avons eu le message :

`[WARNING IsDockerSystemdCheck]: detected "cgroupfs" as the Docker cgroup driver. The recommended driver is "systemd". Please follow the guide at https://kubernetes.io/docs/setup/cri/`

```
cat <<EOF | sudo tee /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
```



```shell
cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF

sudo sysctl --system
```

Résultat : 

```
* Applying /etc/sysctl.d/99-kubernetes-cri.conf ...
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
* Applying /etc/sysctl.d/99-sysctl.conf ...
kernel.panic = 10
net.ipv6.conf.all.disable_ipv6 = 1
* Applying /etc/sysctl.d/protect-links.conf ...
fs.protected_hardlinks = 1
fs.protected_symlinks = 1
* Applying /etc/sysctl.conf ...
kernel.panic = 10
net.ipv6.conf.all.disable_ipv6 = 1
```

Désormais nous pouvons passer à l'installation de Calico.
