# Installation d'un Storage Provisioner

## Application du Local Path Provisioner

`kubectl apply -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/deploy/local-path-storage.yaml`

## Vérification post installation

```
Thibault@vmi467043:~/nginx-ingress/tuto$ kubectl -n local-path-storage get pod
NAME                                      READY   STATUS    RESTARTS   AGE
local-path-provisioner-5696dbb894-7prfm   1/1     Running   0          7s
```
