# Installation de Harbor

## Ajout du repo

`helm repo add harbor https://helm.goharbor.io`

## Installation de la chart

`helm install harbor harbor/harbor`
