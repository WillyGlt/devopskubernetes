# Installation de Minio

## Ajout de repo HELM

`helm repo add minio https://helm.min.io/`

## Installation de la chart

`helm install --generate-name minio/minio`

`export POD_NAME=$(kubectl get pods --namespace default -l "release=minio-1607630693" -o jsonpath="{.items[0].metadata.name}") `

`kubectl port-forward $POD_NAME 9000 --namespace default`
