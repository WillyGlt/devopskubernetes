# Mise en place de quotas

Notre VPS possède 3 coeurs, 15,7 Go de mémoire vive et 400 Go d'espace disque.

## Nginx 

`kubectl apply -f quota-wordpress.yaml --namespace=nginx`

## Monitoring

`kubectl apply -f quota-monitoring.yaml --namespace=monitoring`

## Wordpress

`kubectl apply -f quota-wordpress.yaml --namespace=wordpress`

## Database

`kubectl apply -f quota-database.yaml --namespace=database`

## Registry

`kubectl apply -f quota-registry.yaml --namespace=registry`

## Koel

`kubectl apply -f quota-koel.yaml --namespace=koel`
