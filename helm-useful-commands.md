 # Commandes utiles pour HELM

 ## Lister les releases HELM

 ```
helm ls -a
NAME    NAMESPACE       REVISION        UPDATED                                 STATUS  CHART                   APP VERSION
nginx   default         1               2020-11-16 14:56:38.744465556 +0100 CET failed  ingress-nginx-3.10.1    0.41.2
 ```

## Supprimer une release

```
helm uninstall nginx
release "nginx" uninstalled
```

## Lister les repos 

```
helm repo ls
NAME            URL
ingress-nginx   https://kubernetes.github.io/ingress-nginx
```

## Supprimer un repo

```
helm repo rm ingress-nginx
"ingress-nginx" has been removed from your repositories
```
