# Création des namespaces

## Nginx

```
kubectl create namespace nginx
namespace/nginx created
```

## Monitoring

```
kubectl create namespace monitoring
namespace/monitoring created
```

## Wordpress

```
kubectl create namespace wordpress
namespace/wordpress created
```

## Registry

```
kubectl create namespace registry
namespace/registry created
```

## Database

```
kubectl create namespace database
namespace/database created
```

## Koel

```
kubectl create namespace koel
namespace/koel created
```

## RBAC

```
kubectl create namespace rbac
namespace/rbac created
```

## Vérifications 

```
kubectl get namespaces

NAME              STATUS   AGE
calico-system     Active   6m7s
default           Active   7m17s
koel              Active   2m17s
kube-node-lease   Active   7m19s
kube-public       Active   7m19s
kube-system       Active   7m19s
monitoring        Active   2m33s
nginx             Active   2m42s
registry          Active   2m21s
tigera-operator   Active   6m17s
wordpress         Active   2m25s
```
